<div align="center">
  <div>
    <h1>ramfetch</h1>
    <p>A fetch which displays memory info using /proc/meminfo.</p>
    <img src="https://img.shields.io/gitlab/license/o69mar/ramfetch?style=flat-square&logo=license">
    <img src="https://shields.io/badge/made-with%20%20sh-green?style=flat-square&color=d5c4a1&labelColor=1d2021">
    <img src="https://img.shields.io/gitlab/forks/o69mar/ramfetch?style=flat-square">
    <img src="https://img.shields.io/gitlab/stars/o69mar/ramfetch?style=flat-square">
    <img src="https://img.shields.io/gitlab/forks/o69mar/ramfetch?style=flat-square">
    <img src="https://img.shields.io/aur/version/ramfetch?style=flat-square&logo=arch">
  </div>
  <div>
<br>
<img width="670" src="./assets/image.png">
</div>
</div>
<br>

ramfetch is a "fetch" tool which displays memory info using /proc/meminfo. if you want to install ramfetch follow the steps below. it's really simple. you can also test ramfetch without installing it.

ramfetch works on:

&check; Linux

&check; Android, using termux (no root)


## Requirements

`sh` for ramfetch to work.

`make` to install ramfetch. (optional)


## Install

### Run with curl
If you don't wanna install ramfetch/clone this repo. you can run it with curl.
```bash
$ curl https://gitlab.com/o69mar/ramfetch/-/raw/main/ramfetch | sh
```

### AUR
To install ramfetch from the AUR, install it by using your favorite AUR helper. (e.g. yay or paru) thanks to [jahway603](https://gitlab.com/jahway603)

The Arch AUR has `ramfetch` and `ramfetch-git` packages. The `ramfetch` AUR package builds the code from each [release](https://gitlab.com/o69mar/ramfetch/releases) & the `-git` AUR package builds the code from this [git's main branch](https://gitlab.com/o69mar/ramfetch). I wanted to give Archers a choice.
```bash
# use yay
$ yay -S ramfetch-git
# or paru
$ paru -S ramfetch-git
```

### Gitlab
Clone this repo.
```bash
$ git clone https://gitlab.com/o69mar/ramfetch
```
Install ramfetch using `make install`.
```bash
# make install
```

## Reinstall
Reinstall ramfetch using `make`.
```bash
# make reinstall
```

## Uninstall
Uninstall ramfetch using `make`.
```bash
# make uninstall
```
