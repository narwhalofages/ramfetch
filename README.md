<div align="center">
  <div>
    <h1>ramfetch</h1>
    <p>A fetch which displays memory info using /proc/meminfo.</p>
  </div>
  <div>
<br>
<img width="670" src="./assets/image.png">
</div>
</div>
<br>

ramfetch is a "fetch" tool which displays memory info using /proc/meminfo. if you want to install ramfetch follow the steps below. it's really simple. you can also test ramfetch without installing it.

ramfetch works on:

&check; Linux

&check; Android, using termux (no root)


## Requirements

`sh` for ramfetch to work.

`make` to install ramfetch. (optional)


## Install

### Run with curl
If you don't wanna install ramfetch/clone this repo. you can run it with curl.
```bash
$ curl https://codeberg.org/o69mar/ramfetch/raw/branch/main/ramfetch | sh
```

### AUR

**WARNING: The aur package is broken, because it clones the old github repo.**

To install ramfetch from the AUR, install it by using your favorite AUR helper. (e.g. yay or paru) thanks to [jahway603](https://gitlab.com/jahway603)

```bash
# use yay
$ yay -S ramfetch-git
# or paru
$ paru -S ramfetch-git
```

### Codeberg
Clone this repo.
```bash
$ git clone https://codeberg.org/o69mar/ramfetch
```
Install ramfetch using `make install`.
```bash
# make install
```

## Reinstall
Reinstall ramfetch using `make`.
```bash
# make reinstall
```

## Uninstall
Uninstall ramfetch using `make`.
```bash
# make uninstall
```
